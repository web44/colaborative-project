import React from 'react'

import MainMenu from '../Molecules/MainMenu'
import Footer from '../Molecules/Footer'

const Rules = () => {
    return(
        <>
        <div className="container">
        <MainMenu/>
        <div className="ed-grid rules">
            <h1>Reglas de uso</h1>
            <p>
                El presente sitio web es un projecto de código abierto y software libre sin fines de lucro basado en la idea de compartir compartir projectos y 
                herramientas creadas por la comunidad hispana de desarrolladores.
            </p>
            <ol>
                <li>Respeto. Se pide a los participantes del projecto respetar a los demás participantes y usuarios</li>
                <li>Mantener la temática del esspacio. El projecto está basado en la idea de compartir projectos de software y tecnologia, se pide mantener esta temática en el espacio</li>
                <li>Mantener buenas prácticas en el desarrollo y procurar que las publicaciones de los proyectos sean accesibles y tengan un texto alternativo adecuado</li>
                <li>No publicar recursos de pago o que inflinjan derechos de autor. Se permite la publicación de projectos comerciales pero siempre y cuando no se inflinja ninguna norma legal y estén basados en código abierto o software libre</li>
            </ol>
            <p>Gracias por participar en este proyecto</p>
        </div>
        <Footer />
        </div>
        </>
    )
}

export default Rules