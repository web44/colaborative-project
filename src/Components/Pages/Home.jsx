import React from 'react'

import MainMenu from '../Molecules/MainMenu'
import Header from '../Molecules/Header'
import MainSection from '../Molecules/MainSection'
import Footer from '../Molecules/Footer'

const Home = () => {
    return(
        <>
            <MainMenu />
            <Header />
            <main>
                <MainSection title="Proyectos" text="Proyectos de código abierto y software libre para explorar y colaborar." linkButton="/colaborative-project/proyectos"className="main-section--projects" />
                <MainSection title="Herramientas" linkButton="/colaborative-project/herramientas" text="Herramientas para desarrolladores, libros, cursos y recursos libres." className="main-section--tools"/>
            </main>
            <Footer />
        </>
    )
}

export default Home