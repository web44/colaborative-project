import React from 'react'
import { Link } from 'react-router-dom'
import propTypes from 'prop-types'

const MainSection = ({title, text, textButton, linkButton, className}) => {
    return(
        <section className={className}>
            <div className="main-section__text-container">
                <h2>{title}</h2>
                <p>{text}</p>
                <Link to={linkButton} className="button">{textButton}</Link>
            </div>
        </section>
    )
}

MainSection.propTypes = {
    title: propTypes.string,
    textButton: propTypes.string,
    linkButton: propTypes.string,
    className: propTypes.string,
}

MainSection.defaultProps = {
    title: " ",
    textButton: "Ver más",
    linkButton: "#"
}

export default MainSection