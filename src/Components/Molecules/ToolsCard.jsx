import React from 'react'

const ToolsCard = ({name, description, src, image, alt}) => {
    return (
        <div className="project-card">
            <div className="project-card__image-container">
                <img src={image} alt={alt} className="project-card__image" />
            </div>
            <div className="project-card__text-container">
                <h3 className="project-card__title">{name}</h3>
                <p className="project-card__description">
                    {description}
                </p>
                <a href={src} className="project-card__link">Ver herramienta</a>
            </div>
        </div>
    )
}

export default ToolsCard