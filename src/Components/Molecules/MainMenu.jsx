import React from 'react'
import { NavLink } from 'react-router-dom'

import '../../sass/style.scss'

const MainMenu = () => {
    return(
        <nav className="main-menu">
        <div className="main-menu__toggle"></div>
            <ul className="main-menu__list">
                <li className="main-menu__item">
                    <NavLink to="/colaborative-project" className="main-menu__link">Inicio</NavLink>
                </li>
                <li className="main-menu__item">
                    <NavLink to="/colaborative-project/proyectos" className="main-menu__link">Proyectos</NavLink>
                </li>
                <li className="main-menu__item">
                    <NavLink to="/colaborative-project/herramientas" className="main-menu__link">Recursos</NavLink>
                </li>
            </ul>
        </nav>
    )
}

export default MainMenu